package com.example.examgateappl;

/**
 * Created by vashisht on 11/3/2015.
 */


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.example.examgateappl.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Tab1 extends Activity implements View.OnClickListener {
    TextView studentName,studentId,course,year;
    private String userid;
    private List courses = new ArrayList();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modules);

        userid = getIntent().getExtras().getString("Student Id");
        getStudentDetails();
        getCourseDetails();
    }

    private void getCourseDetails() {
        /*ConnectionClass connectionClass = new ConnectionClass();
        try {

            Connection con = connectionClass.getConnection();
            if (con == null) {

            }
            else {

                String query = "SELECT Course_Details.Course_Name, Course_Details.Exam_Date, Enrolled_Course.Course_Enrolled FROM "+
                        "Course_Details CROSS JOIN Enrolled_Course WHERE        (Enrolled_Course.Student_ID = 'cs12vvk') AND"+
                        "(Enrolled_Course.Course_Enrolled = Course_Details.Course_ID)";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                TableLayout linearLayout = (TableLayout)findViewById(R.id.Table);
                courses = new ArrayList();
                int id  = 0 ;
                while(rs.next())
                {
                    TableRow row = new TableRow(this);
                    TextView textView1 = new TextView(this);
                    textView1.setText(rs.getString(1));
                    TextView textView2 = new TextView(this);
                    textView2.setText(rs.getString(2));
                    TextView textView3 = new TextView(this);
                    textView3.setText(rs.getString(3));
                    row.addView(textView3);
                    row.addView(textView1);
                    row.addView(textView2);
                    Button button = new Button(this);
                    button.setText("Set Reminder");
                    button.setId(id);
                    courses.add(id,rs.getString(3));
                    id++;
                    button.setOnClickListener(this);
                    row.addView(button);
                    linearLayout.addView(row);

                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        TableLayout linearLayout = (TableLayout)findViewById(R.id.Table);
        int id = 0;
        TableRow row = new TableRow(this);
        TextView textView1 = new TextView(this);
        textView1.setText("Test");
        TextView textView2 = new TextView(this);
        textView2.setText("Test");
        TextView textView3 = new TextView(this);
        textView3.setText("Test");
        row.addView(textView3);
        row.addView(textView1);
        row.addView(textView2);
        Button button = new Button(this);
        button.setText("Set Reminder");
        button.setId(id);
        courses.add(id,"Test");
        id++;
        button.setOnClickListener(this);
        row.addView(button);
        linearLayout.addView(row);


    }

    private void getStudentDetails() {
        /*ConnectionClass connectionClass = new ConnectionClass();
        try {

            Connection con = connectionClass.getConnection();
            if (con == null) {

            } else {
                String query = "select * from dbo.Student_Record where Student_ID='" + userid +  "'";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                if(rs.next())
                {
                    studentName = (TextView) findViewById(R.id.Name);
                    studentId = (TextView) findViewById(R.id.ID);
                    course = (TextView) findViewById(R.id.Course);
                    year = (TextView) findViewById(R.id.Year);
                    studentName.setText("Name: "+rs.getString("Name"));
                    studentId.setText("Student Id: "+ rs.getString("Student_ID"));
                    course.setText("Course: "+rs.getString("Course"));
                    year.setText("Year: "+ rs.getString("Year"));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }*/
        studentName = (TextView) findViewById(R.id.Name);
        studentId = (TextView) findViewById(R.id.ID);
        course = (TextView) findViewById(R.id.Course);
        year = (TextView) findViewById(R.id.Year);
        studentName.setText("Name: "+"Vashisth Kapoor");
        studentId.setText("Student Id: "+ "1238622");
        course.setText("Course: "+"Test");
        year.setText("Year: "+ "2012");
    }

    @Override
    public void onClick(View v) {
        /*CalenderHelper c = new CalenderHelper();
        c.initActivityObj(this);
        String courseId = String.valueOf(courses.get(v.getId()));*/
        Intent i = new Intent(Tab1.this, iNavigate.class);
        i.putExtra("Student Id",userid);
        startActivity(i);
        finish();
    }
}