package com.example.examgateappl;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.examgateappl.ConnectionClass;
import com.example.examgateappl.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class MyActivity extends Activity {

    ConnectionClass connectionClass;
    EditText edtuserid,edtpass;
    Button btnlogin;
    ProgressBar pbbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        connectionClass = new com.example.examgateappl.ConnectionClass();
        edtuserid = (EditText) findViewById(R.id.LoginId);
        edtpass = (EditText) findViewById(R.id.Password);
        btnlogin = (Button) findViewById(R.id.Login_Button);
        pbbar = (ProgressBar) findViewById(R.id.pbbar);
        pbbar.setVisibility(View.GONE);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoLogin doLogin = new DoLogin();
                doLogin.execute("");
            }
        });
    }
    public class DoLogin extends AsyncTask<String,String,String>
    {
        String z = "";
        Boolean isSuccess = false;
        String userid = edtuserid.getText().toString();
        String password = edtpass.getText().toString();
        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);
        }
        @Override
        protected void onPostExecute(String r) {
            pbbar.setVisibility(View.GONE);
            Toast.makeText(MyActivity.this,r,Toast.LENGTH_SHORT).show();
            if(isSuccess) {
                Intent i = new Intent(MyActivity.this, AddProducts.class);
                i.putExtra("Student Id",userid);
                startActivity(i);
                finish();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            /*if(userid.trim().equals("")|| password.trim().equals(""))
                z = "Please enter User Id and Password";
            else
            {
                try {
                    Connection con = connectionClass.getConnection();
                    if (con == null) {
                        z = "Error in connection with SQL server";
                    } else {
                        String query = "select * from dbo.Id_Fact_Password where StudentID='" + userid + "' and Password='" + password + "'";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);
                        if(rs.next())
                        {
                            z = "Login successfull";
                            isSuccess=true;
                        }
                        else
                        {
                            z = "Invalid Credentials";
                            isSuccess = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = "Exceptions";
                }
            }*/
            isSuccess=true; // remove when server works
            z = "Login successfull";
            return z;
        }
    }
}