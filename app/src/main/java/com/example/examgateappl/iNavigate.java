package com.example.examgateappl;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.example.examgateappl.R;

/**
 * Created by vashisht on 12/31/2015.
 */
public class iNavigate extends Activity {


        ListView listView ;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.inav);

            // Get ListView object from xml
            listView = (ListView) findViewById(R.id.listView);

            // Defined Array values to show in ListView
            String[] values = new String[] { "Exam Hall",
                    "Lecture Center",
                    "John Crank",
                    "St. James",

            };

            // Define a new Adapter
            // First parameter - Context
            // Second parameter - Layout for the row
            // Third parameter - ID of the TextView to which the data is written
            // Forth - the Array of data

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, values);


            // Assign adapter to ListView
            listView.setAdapter(adapter);

            // ListView Item Click Listener
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    // ListView Clicked item index
                    int itemPosition     = position;

                    // ListView Clicked item value
                    String  itemValue    = (String) listView.getItemAtPosition(position);
                    Intent i = new Intent(iNavigate.this, MapsActivity.class);
                    i.putExtra("Room Number",itemValue);
                    startActivity(i);
                    finish();

                }

            });
        }

    }