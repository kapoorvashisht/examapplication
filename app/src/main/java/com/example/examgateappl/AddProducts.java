package com.example.examgateappl;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.example.examgateappl.R;

/*
 * Created by vashisht on 11/3/2015.
 */
public class AddProducts extends TabActivity {

    private TabHost mTabHost;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabbedactivity);
        TabHost tabHost = getTabHost();
        String id= getIntent().getExtras().getString("Student Id");
        // Tab for Photos
        TabHost.TabSpec modules = tabHost.newTabSpec("Modules Enrolled");
        // setting Title and Icon for the Tab
        modules.setIndicator("Modules Enrolled ");
        Intent photosIntent = new Intent(this, Tab1.class);
        photosIntent.putExtra("Student Id",id);
        modules.setContent(photosIntent);

        // Tab for Songs
        TabHost.TabSpec nextExam = tabHost.newTabSpec("Next Exam");
        nextExam.setIndicator("Next Exam");
        Intent songsIntent = new Intent(this, Tab2.class);
        nextExam.setContent(songsIntent);

        // Adding all TabSpec to TabHost
        tabHost.addTab(modules); // Adding photos tab
        tabHost.addTab(nextExam); // Adding songs tab
    }
}